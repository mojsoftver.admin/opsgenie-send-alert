#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/opsgenie-send-alert"}

  echo "Building image $DOCKER_IMAGE..."
  run docker build -t ${DOCKER_IMAGE} .
}

@test "Alert is sent successfully" {

    MESSAGE="Pipelines is awesome: $RANDOM!"

    run docker run \
        -e GENIE_KEY=$GENIE_KEY \
        -e MESSAGE="$MESSAGE" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [[ "$output" == *"Follow this link"* ]]

    sleep 3

    response=$(curl -XGET -H "Authorization: GenieKey $GENIE_KEY" 'https://api.opsgenie.com/v2/alerts?query=status%3Aopen&limit=1&sort=createdAt&order=desc' | jq -r '.data[0].message')

    echo "Response: $response"

    [ "$status" -eq 0 ]
    [ "$response" = "$MESSAGE" ]
}

@test "Link to alert is present" {
    skip "TODO check alert_id in the output"

    MESSAGE="Pipelines is awesome: $RANDOM!"

    run docker run \
        -e GENIE_KEY=$GENIE_KEY \
        -e MESSAGE="$MESSAGE" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    alert_id=$(curl -XGET -H "Authorization: GenieKey $GENIE_KEY" 'https://api.opsgenie.com/v2/alerts?query=status%3Aopen&limit=1&sort=createdAt&order=desc' | jq -r '.data[0].id')

    echo "Response: $response"
    echo "Alert id: $alert_id"

    [ "$status" -eq 0 ]
    [[ "$output" =~ "https://app.opsgenie.com/alert/detail/$alert_id/details" ]]
}

@test "Required parameter GENIE_KEY missing" {

    MESSAGE="Pipelines is awesome: $RANDOM!"

    run docker run \
        -e MESSAGE="$MESSAGE" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"


    [[ "$status" -ne 0 ]]
    [[ "$output" == *"GENIE_KEY variable missing"* ]]
}

@test "Required parameter MESSAGE missing" {

    run docker run \
        -e GENIE_KEY=$GENIE_KEY \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -ne 0 ]]
    [[ "$output" == *"MESSAGE variable missing"* ]]
}

@test "Alert is sent successfully with double quote in message" {

    MESSAGE="Pipelines is awesome: \" $RANDOM!"

    run docker run \
        -e GENIE_KEY=$GENIE_KEY \
        -e MESSAGE="$MESSAGE" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    sleep 3

    response=$(curl -XGET -H "Authorization: GenieKey $GENIE_KEY" 'https://api.opsgenie.com/v2/alerts?query=status%3Aopen&limit=1&sort=createdAt&order=desc' | jq -r '.data[0].message')

    echo "Response: $response"

    [ "$status" -eq 0 ]
    [ "$response" = "$MESSAGE" ]
}
